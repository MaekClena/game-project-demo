﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace GameExternalResources
{
    [Serializable]
    public class CharacterDefinition
    {
        //public Vector2 Position { get; set; }
        public sbyte Direction { get; set; }
        public string IdleAnimationLocation { get; set; }
        public string WalkingAnimationLocation { get; set; }
        public List<Rectangle> HitBox { get; set; }
        public int StepHeight { get; set; }
        public float MaxForwardSpeed { get; set; } //u.ms^-1
        public float MaxBackwardSpeed { get; set; } //u.ms^-1
        public float MaxFallingSpeed { get; set; } //u.ms^-1
        public float AirSpeed { get; set; } //u.ms^-1
        public float Acceleration { get; set; } //u.ms^-2
        public float Deceleration { get; set; } //u.ms^-2
    }
}
