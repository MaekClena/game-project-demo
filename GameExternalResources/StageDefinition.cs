﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameExternalResources
{
    [Serializable]
    public class StageDefinition
    {
        public string NextStageLocation { get; set; }
        public string CharacterDefinitionLocation { get; set; }
        public Vector2 CharacterInitialPosition { get; set; }
        public string TileMapLocation { get; set; }
        public float Gravity { get; set; }
    }
}
