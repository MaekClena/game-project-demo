﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameExternalResources
{
    [Serializable]
    public class Circle
    {
        public ushort Radius { get; set; }
        public Vector2 Center { get; set; }
    }
}
