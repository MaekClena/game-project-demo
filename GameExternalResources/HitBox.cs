﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameExternalResources;
using Microsoft.Xna.Framework.Content;

namespace GameExternalResources
{
    [Serializable]
    public class HitBox
    {
        [ContentSerializer(Optional = true)]
        public List<Rectangle> Rectangles { get; set; }
        [ContentSerializer(Optional = true, FlattenContent = true, CollectionItemName = "Circle")]
        public List<Circle> Circles { get; set; }
        [ContentSerializerIgnore]
        public ushort Height { get; set; }
        [ContentSerializerIgnore]
        public ushort Width { get; set; }

        public void Init()
        {
            Width = ushort.MinValue;
            Height = ushort.MinValue;
            foreach (Circle Circle in Circles)
            {
                ushort width = (ushort)(Circle.Center.X + Circle.Radius);
                if (width > Width)
                {
                    Width = width;
                }
                ushort height = (ushort)(Circle.Center.Y + Circle.Radius);
                if (height > Height)
                {
                    Height = height;
                }
            }
            foreach (Rectangle rectangle in Rectangles)
            {
                ushort width = (ushort)rectangle.Right;
                if (width > Width)
                {
                    Width = width;
                }
                ushort height = (ushort)rectangle.Bottom;
                if (height > Height)
                {
                    Height = height;
                }
            }
        }

        /*public void Offset(int offsetX, int offsetY)
        {
            foreach (Circle circle in Circles)
            {
                circle.Center = new Vector2(circle.Center.X + offsetX, circle.Center.Y + offsetY);
            }
            foreach (Rectangle rectangle in Rectangles)
            {
                rectangle.Offset(offsetX, offsetY);
            }
        }

        public bool collision(Vector2 position)
        {
            return false;
        }*/

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphics, Vector2 position)
        {
            var t = new Texture2D(graphics, 1, 1);
            t.SetData(new[] { Color.White });
            foreach (Rectangle Rectangle in Rectangles)
            {
                spriteBatch.Draw(t, new Rectangle((int)position.X + Rectangle.X, (int)position.Y + Rectangle.Y, Rectangle.Width, Rectangle.Height),  Color.Red);
            }
            foreach (Circle Circle in Circles)
            {
                spriteBatch.Draw(t, new Rectangle((int)position.X + (int)Circle.Center.X - Circle.Radius, (int)position.Y + (int)Circle.Center.Y - Circle.Radius, 2 * Circle.Radius, 2 * Circle.Radius), Color.Blue);
            }
        }
    }
}
