﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameExternalResources
{
    [Serializable]
    public class GameSettings
    {
        public Setting MusicVolume { get; set; }
        public Setting EffectsVolume { get; set; }
        public Vector2 Resolution { get; set; }
        public bool FullScreen { get; set; }
    }
}
