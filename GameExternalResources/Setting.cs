﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameExternalResources
{
    [Serializable]
    public class Setting
    {
        public float Value { get; set; }
        /*{ 
            get { return Value; }
            set { Value = Regulate(value); }
        }*/
        public float ValueMax { get; set; }
        public float ValueMin { get; set; }
        static float Step = 0.05f;

        private float Regulate(float value)
        {
            if (value > ValueMax)
            {
                return ValueMax;
            }
            else if (value < ValueMin)
            {
                return ValueMin;
            }
            return value;
        }

        public void Increase()
        {
            Value = Math.Min(Value + Step, ValueMax);
        }

        public void Decrease()
        {
            Value = Math.Max(Value - Step, ValueMin);
        }
    }
}
