using System;
using System.Runtime.InteropServices;
using WiredPrairie.Utilities;
using System.Windows.Forms;

namespace GameApplication
{
#if WINDOWS
    static class Program
    {
        static void ShowExceptionBox(Exception e)
        {
            MessageBox.Show(e.GetType().Name + "\n" + e.Message + "\n\nThe program will now exit.", "Fatal error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            //Splashing Splasher = new Splashing("lib\\TheSplash.dll", typeof(Program).Module, 101);
            //Splasher.Show();
            using (GameApplication game = new GameApplication())
            {
                //try
                {
                    //Splasher.Close();
                    game.Run();
                }
                /*catch (Exception e)
                {
                    game.IsMouseVisible = true;
                    ShowExceptionBox(e);
                    game.Exit();
                }*/
            }
        }
    }
#endif
}

