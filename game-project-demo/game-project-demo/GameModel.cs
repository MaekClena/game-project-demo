﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;
using GraphicalUserInterface;
using GameAssets;
using GameExternalResources;
using Microsoft.Xna.Framework.Input;
using Constants;

namespace GameApplication
{
    public enum GameState { Loading, Menu, Stage, Cutscene };

    public class GameModel : GameComponent
    {
        public GameState CurrentState { get; set; }
        public int FrameRate { get; set; }
        public int TotalFrame { get; set; }
        private float ElapsedTime;
        public GameSettings GameSettings { get; set; }
        public MainMenu MainMenu { get; set; }
        public PauseMenu PauseMenu { get; set; }
        public Stage Stage { get; set; }
        public Pointer Pointer { get; set; }
        

        public GameModel(Game game, GameSettings gameSettings, Dictionary<String, Keys> keyBinds)
            : base(game)
        {
            ElapsedTime = 0.0f;
            TotalFrame = 0;
            CurrentState = GameState.Menu;
            GameSettings = gameSettings;
            Pointer = new Pointer(game);

            MainMenu = new MainMenu(Game, LoadSave, SaveChanges, GameSettings, keyBinds);
            Stage = null;
        }

        public void LoadStage(StageDefinition stageDefinition)
        {
            CurrentState = GameState.Stage;
            Stage = new Stage(Game, stageDefinition, () => CurrentState = GameState.Menu, GameSettings);
        }

        public void LoadSave()
        {
            String StageDefinitionLocation;

            if (Stage != null)
            {
                StageDefinitionLocation = Stage.NextStageLocation;
            }
            else
            {
                if (!File.Exists(Game.Content.RootDirectory + Parameters.SaveFileLocation))
                {
                    using (StreamWriter sw = new StreamWriter(Game.Content.RootDirectory + Parameters.SaveFileLocation))
                    {
                        StageDefinitionLocation = Parameters.FirstStageLocation;
                        sw.WriteLine(StageDefinitionLocation);
                        sw.Close();
                    }
                }
                else
                {
                    using (StreamReader sr = new StreamReader(Game.Content.RootDirectory + Parameters.SaveFileLocation))
                    {
                        StageDefinitionLocation = sr.ReadLine();
                        sr.Close();
                    }
                }
            }
            LoadStage(Game.Content.Load<StageDefinition>(StageDefinitionLocation));
        }

        public void SaveChanges()
        {
            //write all xml
        }

        public override void Update(GameTime gameTime)
        {
            ElapsedTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (ElapsedTime >= 1000.0f)
            {
                FrameRate = TotalFrame;
                TotalFrame = 0;
                ElapsedTime = 0;
            }
            switch (CurrentState)
            {
                case GameState.Stage : 
                    Stage.Update(gameTime);
                    break;
                case GameState.Menu : 
                    //menu.Update(gameTime);
                    break;
            }
        }
    }
}
