﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using GraphicalUserInterface;
using GameAssets;

namespace GameApplication
{
    public class GameController : GameComponent
    {
        private GameModel Model;
        public Dictionary<string, Keys> KeyBinds { get; set; }

        private MouseState PreviousMouseState;
        private MouseState CurrentMouseState;
        private KeyboardState CurrentKeyboardState;
        private KeyboardState PreviousKeyboardState;

        public GameController(Game game, GameModel model, Dictionary<string, Keys> keyBinds)
            : base(game)
        {
            Model = model;
            KeyBinds = keyBinds;
            PreviousMouseState = new MouseState();
            CurrentMouseState = Mouse.GetState();
            CurrentKeyboardState = new KeyboardState();
            PreviousKeyboardState = Keyboard.GetState();
        }

        public override void Update(GameTime gameTime)
        {
            PreviousMouseState = CurrentMouseState;
            CurrentMouseState = Mouse.GetState();
            PreviousKeyboardState = CurrentKeyboardState;
            CurrentKeyboardState = Keyboard.GetState();

            Model.Pointer.Position = new Vector2(CurrentMouseState.X, CurrentMouseState.Y);

            switch (Model.CurrentState)
            {
                case GameState.Menu:
                    Model.MainMenu.ReadInput(CurrentMouseState, PreviousMouseState, CurrentKeyboardState, PreviousKeyboardState);
                    break;
                case GameState.Stage:
                    Stage Stage = Model.Stage;
                    switch (Stage.State)
                    {
                        case StageState.Running:
                            if (CurrentKeyboardState.GetPressedKeys().Length > 0)
                            {
                                if (CurrentKeyboardState.IsKeyDown(Keys.Escape) && PreviousKeyboardState.IsKeyUp(Keys.Escape))
                                {
                                    Stage.State = StageState.Paused;
                                }
                                Stage.Character.ReadInputs(KeyBinds, CurrentMouseState, PreviousMouseState, CurrentKeyboardState, PreviousKeyboardState);
                            }
                                /*if (currentKeyboardState.IsKeyDown(KeyBinds[KeyControls.Forward]))
                                {
                                    Stage.Character.CurrentHorizontalState = HorizontalState.Forward;
                                    //Stage.Character.ToHorizontalState("Walk");
                                }
                                if (currentKeyboardState.IsKeyDown(KeyBinds[KeyControls.Backward]))
                                {
                                    Stage.Character.CurrentHorizontalState = HorizontalState.Backward;
                                    //Stage.Character.ToHorizontalState("Back");
                                }
                                if (currentKeyboardState.IsKeyDown(KeyBinds[KeyControls.Up]) && previousKeyboardState.IsKeyUp(KeyBinds[KeyControls.Up]))
                                {
                                    Stage.Character.CurrentVerticalState = VerticalState.Up;
                                    //Stage.Character.ToVerticalState("Jump");
                                }
                                if (Stage.Character.CurrentHorizontalState != HorizontalState.Idle && Stage.Character.CurrentVerticalState == VerticalState.Idle) //doesn't quite work
                                //if (Stage.Character.CurrentHorizontalState != "Idle" && Stage.Character.CurrentVerticalState == "Idle")
                                {
                                    float brakeDistance = (Stage.Character.Velocity.X * Stage.Character.Velocity.X) / (2 * Stage.Character.Deceleration); //v^2/2a
                                    if (Stage.Character.Direction == 1 && currentMouseState.X < Stage.Character.Position.X + Stage.Character.HitBox[0].Width/2 - brakeDistance
                                        || Stage.Character.Direction == -1 && currentMouseState.X > Stage.Character.Position.X + Stage.Character.HitBox[0].Width / 2 + brakeDistance)
                                    {
                                        Stage.Character.CurrentHorizontalState = HorizontalState.Stopping;
                                        //Stage.Character.ToHorizontalState("Stop");
                                    }
                                }
                                //if (stage.character.currentHState == CharacterState.IDLE)
                                {
                                    if ((Stage.Character.Direction == 1) && currentMouseState.X < Stage.Character.Position.X + Stage.Character.HitBox[0].Width)
                                    {
                                        Stage.Character.Direction = -1;
                                    }
                                    else if (Stage.Character.Direction == -1 && currentMouseState.X > Stage.Character.Position.X)
                                    {
                                        Stage.Character.Direction = 1;
                                    }
                                }
                                //stage.character.right = (!stage.character.right && currentMouseState.X >= stage.character.hitBox.Left) || (stage.character.right && currentMouseState.X > stage.character.hitBox.Right);
                                /*else
                                {
                                    stage.character.currentState = CharacterState.IDLE;
                                }
                            }
                            else if (Stage.Character.CurrentHorizontalState != HorizontalState.Idle)
                            //else if (Stage.Character.CurrentHorizontalState != "Idle")
                            {
                                Stage.Character.CurrentHorizontalState = HorizontalState.Stopping;
                                //Stage.Character.ToHorizontalState("Stop");
                            }*/
                            break;
                        case StageState.Paused:
                            if (CurrentKeyboardState.IsKeyDown(Keys.Escape) && PreviousKeyboardState.IsKeyUp(Keys.Escape))
                            {
                                Stage.State = StageState.Running;
                            }
                            Stage.PauseMenu.ReadInput(CurrentMouseState, PreviousMouseState, CurrentKeyboardState, PreviousKeyboardState);
                            break;
                    }
                    break;
            }
        }
    }
}
