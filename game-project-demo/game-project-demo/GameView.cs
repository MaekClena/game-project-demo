﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using GameExternalResources;

namespace GameApplication
{
    public class GameView : DrawableGameComponent
    {
        private GameModel Model;

        public GameView(Game game, GameModel model)
            : base(game)
        {
            Model = model;
        }
        
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            switch (Model.CurrentState)
            {
                case GameState.Menu : 
                    Model.MainMenu.Draw(gameTime, spriteBatch);
                    break;
                case GameState.Stage : 
                    Model.Stage.Draw(gameTime, spriteBatch);
                    break;
                case GameState.Cutscene : 
                    break;
            }
            Model.Pointer.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }
    }
}
