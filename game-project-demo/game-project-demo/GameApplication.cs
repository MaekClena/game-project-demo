using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;
using GameExternalResources;
using GameAssets;
using Constants;

namespace GameApplication
{
    public class GameApplication : Game
    {
        public GraphicsDeviceManager GraphicsDeviceManager { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        public GameModel Model { get; private set; }
        public GameView View { get; private set; }
        public GameController Controller { get; private set; }

        public GameSettings GameSettings { get; set; }
        public Dictionary<String, Keys> KeyBinds { get; set; }

        public GameApplication()
        {
            GraphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            GameSettings = Content.Load<GameSettings>(@Parameters.SettingsLocation);
            KeyBinds = Content.Load<Dictionary<String, Keys>>(@Parameters.KeyBindsLocation);

            Model = new GameModel(this, GameSettings, KeyBinds);
            View = new GameView(this, Model);
            Controller = new GameController(this, Model, KeyBinds);

            Components.Add(Model);
            Components.Add(View);
            Components.Add(Controller);

            IsFixedTimeStep = false;
            IsMouseVisible = false;
            Window.Title = "Game Physics Demo";
            GraphicsDeviceManager.PreferredBackBufferWidth = (int)GameSettings.Resolution.X;
            GraphicsDeviceManager.PreferredBackBufferHeight = (int)GameSettings.Resolution.Y;
            GraphicsDeviceManager.IsFullScreen = GameSettings.FullScreen;
            GraphicsDeviceManager.ApplyChanges();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            base.LoadContent();
        }

        protected override void UnloadContent()
        {
            Content.Unload();
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            View.Draw(gameTime, SpriteBatch);
            base.Draw(gameTime);
        }
    }
}
