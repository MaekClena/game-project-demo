﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Constants;

namespace GameAssets
{
    public class Pointer : DrawableGameComponent
    {
        private Texture2D PointerTexture;
        public Vector2 Position;

        public Pointer(Game game)
            : base(game)
        {
            Position = new Vector2();
            LoadContent();
        }

        protected override void LoadContent()
        {
            PointerTexture = Game.Content.Load<Texture2D>(@Parameters.PointerTextureLocation);
        }

        public override void Update(GameTime gameTime)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(PointerTexture, new Vector2(Position.X - 12, Position.Y - 12), Color.White);
        }
    }
}
