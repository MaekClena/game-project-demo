﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameAssets
{
    public enum VerticalState { Idle, Up, Down };

    interface IMovable
    {
        VerticalState VerticalState { get; set; }
        Vector2 Velocity { get; set; }
        float MaxDownSpeed { get; set; } //u.ms^-1
    }
}
