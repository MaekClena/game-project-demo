﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Demina;
using System.Diagnostics;
using GameExternalResources;
using Microsoft.Xna.Framework.Input;

namespace GameAssets
{
    //public enum CharacterHorizontalState { Idle, Running, Walking, Backward, Stopping };
    //public enum CharacterVerticalState { Idle, Jumping, Falling };

    //public class PlayableCharacter : DrawableGameComponent

    public class Character : Entity, IMovable, IPlayable
    {
        //I AM HERE : where to put the pointer, character? stage? controller?

        //public Vector2 Position { get; set; }
        //public sbyte Direction { get; set; }
        public string IdleAnimationLocation { get; set; }
        public string WalkingAnimationLocation { get; set; }
        //public string HitBoxLocation { get; set; }
        public float MaxForwardSpeed { get; set; } //u.ms^-1
        public float MaxBackwardSpeed { get; set; } //u.ms^-1
        public float MaxDownSpeed { get; set; } //u.ms^-1
        public float AirSpeed { get; set; } //u.ms^-1
        public float Acceleration { get; set; } //u.ms^-2
        public float Deceleration { get; set; } //u.ms^-2

        //main stuff
        public VerticalState VerticalState { get; set; }
        public HorizontalState HorizontalState { get; set; }
        /*public Dictionary<string, CharacterStateAbstract> HorizontalStates { get; set; }
        public Dictionary<string, CharacterStateAbstract> VerticalStates { get; set; }
        public string CurrentVerticalState { get; set; }
        public string CurrentHorizontalState { get; set; }*/
        public AnimationPlayer Animation { get; set; }
        private AnimationManager AnimationManager;
        public Vector2 Velocity { get; set; }
        private Vector2 PointerPosition;

        public List<Rectangle> HitBox { get; set; }
        //public HitBox HitBox { get; set; }
        

        public Character(Game game, Vector2 initialPosition, CharacterDefinition characterDefinition)
            : base(game, initialPosition, characterDefinition.Direction, characterDefinition.HitBox)
        {
            VerticalState = VerticalState.Idle;
            HorizontalState = HorizontalState.Idle;

            /*CurrentHorizontalState = "Idle";
            CurrentVerticalState = "Idle";

            HorizontalStates = new Dictionary<string, CharacterStateAbstract>();
            HorizontalStates.Add("Idle", new HorizontalIdle(this));
            HorizontalStates.Add("Walk", new HorizontalWalk(this));
            HorizontalStates.Add("Back", new HorizontalBack(this));
            HorizontalStates.Add("Stop", new HorizontalStop(this));

            VerticalStates = new Dictionary<string, CharacterStateAbstract>();
            VerticalStates.Add("Idle", new VerticalIdle(this));
            VerticalStates.Add("Fall", new VerticalFall(this));
            VerticalStates.Add("Jump", new VerticalJump(this*/

            //Position = initialPosition;
            //Direction = characterDefinition.Direction;
            IdleAnimationLocation = characterDefinition.IdleAnimationLocation;
            WalkingAnimationLocation = characterDefinition.WalkingAnimationLocation;
            //HitBoxLocation = characterDefinition.HitBoxLocation;
            HitBox = characterDefinition.HitBox;
            MaxForwardSpeed = characterDefinition.MaxForwardSpeed;
            MaxBackwardSpeed = characterDefinition.MaxBackwardSpeed;
            MaxDownSpeed = characterDefinition.MaxFallingSpeed;
            AirSpeed = characterDefinition.AirSpeed;
            Acceleration = characterDefinition.Acceleration;
            Deceleration = characterDefinition.Deceleration;

            Velocity = Vector2.Zero;
            PointerPosition = Vector2.Zero;
            Animation = new AnimationPlayer();
            AnimationManager = new AnimationManager(Animation, HorizontalState);
            LoadContent();
        }

        protected override void LoadContent()
        {
            Animation.AddAnimation(HorizontalState.Idle.ToString(), Game.Content.Load<Animation>(@IdleAnimationLocation));
            Animation.AddAnimation(HorizontalState.Forward.ToString(), Game.Content.Load<Animation>(@WalkingAnimationLocation));
            Animation.StartAnimation(HorizontalState.ToString());
            //HitBox = Game.Content.Load<HitBox>(@HitBoxLocation);
            //HitBox.Init();
        }

        //I AM HERE, put keybinds --v
        public void ReadInputs(Dictionary<string, Keys> keyBinds, MouseState currentMouseState, MouseState previsouMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            PointerPosition = new Vector2(currentMouseState.X, currentMouseState.Y);
            if (currentKeyboardState.IsKeyDown(keyBinds["Forward"]))
            {
                HorizontalState = HorizontalState.Forward;
            }
            if (currentKeyboardState.IsKeyDown(keyBinds["Backward"]))
            {
                HorizontalState = HorizontalState.Backward;
            }
            if (currentKeyboardState.IsKeyDown(keyBinds["Up"]) && previousKeyboardState.IsKeyUp(keyBinds["Up"]))
            {
                VerticalState = VerticalState.Up;
            }
            /*if (HorizontalState != HorizontalState.Idle && VerticalState == VerticalState.Idle) //doesn't quite work
            {
                float brakeDistance = (Velocity.X * Velocity.X) / (2 * Deceleration); //v^2/2a
                if (Direction == 1 && currentMouseState.X < Position.X + HitBox[0].Width / 2 - brakeDistance
                    || Direction == -1 && currentMouseState.X > Position.X + HitBox[0].Width / 2 + brakeDistance)
                {
                    HorizontalState = HorizontalState.Stopping;
                }
            }*/
            if (Direction == 1 && currentMouseState.X < Position.X + HitBox[0].Width)
            {
                Direction = -1;
            }
            else if (Direction == -1 && currentMouseState.X > Position.X)
            {
                Direction = 1;
            }
        }

        public override void Update(GameTime gameTime)
        {
            Animation.Update(gameTime);
            switch (HorizontalState)
            {
                case HorizontalState.Idle:
                    //look stupid
                    break;
                /*case HorizontalState.Stopping: //get rid of that state?
                    if (Velocity.X <= 0)
                    {
                        Velocity = new Vector2(Velocity.X - (Deceleration * gameTime.ElapsedGameTime.Milliseconds), Velocity.Y);
                        if (Velocity.X >= 0)
                        {
                            Velocity = new Vector2(0, Velocity.Y);
                            HorizontalState = HorizontalState.Idle;
                        }
                    }
                    else if (Velocity.X >= 0)
                    {
                        Velocity = new Vector2(Velocity.X + (Deceleration * gameTime.ElapsedGameTime.Milliseconds), Velocity.Y);
                        if (Velocity.X <= 0)
                        {
                            Velocity = new Vector2(0, Velocity.Y);
                            HorizontalState = HorizontalState.Idle;
                        }
                    }
                    break;*/
                case HorizontalState.FastForward:
                    //same as walking with new variables
                    break;
                case HorizontalState.Forward:
                    if (Velocity.X >= MaxForwardSpeed)
                    {
                        Velocity = new Vector2(MaxForwardSpeed, Velocity.Y);
                    }
                    else
                    {
                        Velocity = new Vector2(Velocity.X + (Acceleration * gameTime.ElapsedGameTime.Milliseconds), Velocity.Y);
                    }
                    float forwardBrakeDistance = (Velocity.X * Velocity.X) / (2 * Deceleration); //v^2/2a
                    if (Direction == 1 && PointerPosition.X < Position.X + HitBox[0].Width / 2 - forwardBrakeDistance
                        || Direction == -1 && PointerPosition.X > Position.X + HitBox[0].Width / 2 + forwardBrakeDistance)
                    {
                        Velocity = new Vector2(Velocity.X - (Deceleration * gameTime.ElapsedGameTime.Milliseconds), Velocity.Y);
                        if (Velocity.X >= 0)
                        {
                            Velocity = new Vector2(0, Velocity.Y);
                            HorizontalState = HorizontalState.Idle;
                        }
                    }
                    break;
                case HorizontalState.Backward:
                    if (Velocity.X <= MaxBackwardSpeed)
                    {
                        Velocity = new Vector2(MaxBackwardSpeed, Velocity.Y);
                    }
                    else
                    {
                        Velocity = new Vector2(Velocity.X - (Acceleration * gameTime.ElapsedGameTime.Milliseconds), Velocity.Y);
                    }
                    float backwardBrakeDistance = (Velocity.X * Velocity.X) / (2 * Deceleration); //v^2/2a
                    if (Direction == 1 && PointerPosition.X < Position.X + HitBox[0].Width / 2 - backwardBrakeDistance
                        || Direction == -1 && PointerPosition.X > Position.X + HitBox[0].Width / 2 + backwardBrakeDistance)
                    {
                        Velocity = new Vector2(Velocity.X + (Deceleration * gameTime.ElapsedGameTime.Milliseconds), Velocity.Y);
                        if (Velocity.X <= 0)
                        {
                            Velocity = new Vector2(0, Velocity.Y);
                            HorizontalState = HorizontalState.Idle;
                        }
                    }
                    break;
            }
            switch (VerticalState)
            {
                case VerticalState.Idle:
                    Velocity = new Vector2(Velocity.X, 0);
                    break;
                case VerticalState.Down:
                    if (Velocity.Y >= MaxDownSpeed)
                    {
                        Velocity = new Vector2(Velocity.X, MaxDownSpeed);
                    }
                    else
                    {
                        Velocity = new Vector2(Velocity.X, Velocity.Y + (Acceleration * gameTime.ElapsedGameTime.Milliseconds));
                    }
                    break;
                case VerticalState.Up:
                    if (Velocity.Y == 0)
                    {
                        Velocity = new Vector2(Velocity.X, AirSpeed);
                    }
                    else
                    {
                        Velocity = new Vector2(Velocity.X, Velocity.Y - Deceleration * gameTime.ElapsedGameTime.Milliseconds);
                        if (Velocity.Y >= 0)
                        {
                            VerticalState = VerticalState.Down;
                        }
                    }
                    break;
            }
            //HorizontalStates[CurrentHorizontalState].Update(gameTime);
            //VerticalStates[CurrentVerticalState].Update(gameTime);
            MoveTo(Position.X + Direction * Velocity.X * gameTime.ElapsedGameTime.Milliseconds, Position.Y + Velocity.Y * gameTime.ElapsedGameTime.Milliseconds);
            AnimationManager.Update(gameTime, HorizontalState, VerticalState);
        }

        public void MoveTo(float x, float y)
        {
            Position = new Vector2(x, y);
        }

        public void ToVerticalState(string state)
        {
            //CurrentVerticalState = state;
        }

        public void ToHorizontalState(string state)
        {
            /*CurrentHorizontalState = state;
            if (state == "Idle" || state == "Walk")//Currently handled states, to be removed
            {
                Animation.TransitionToAnimation(state, 0.02f);
            }*/
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var t = new Texture2D(Game.GraphicsDevice, 1, 1);
            t.SetData(new[] { Color.White });
            //HitBox.Draw(spriteBatch, Game.GraphicsDevice, Position);
            spriteBatch.Draw(t, new Rectangle((int)Position.X, (int)Position.Y, HitBox[0].Width, HitBox[0].Height), Color.Red);
            spriteBatch.Draw(t, Position, Color.Yellow);
            spriteBatch.End();
            Animation.Draw(spriteBatch, new Vector2(Position.X + HitBox[0].Width/2, Position.Y + HitBox[0].Height/2), Direction == 1, false, 0, Color.White, new Vector2(1, 1), Matrix.Identity);
            //THANKS M.A. LARSON
            spriteBatch.Begin();
            spriteBatch.Draw(t, Position, Color.Yellow);
        }
    }
}
