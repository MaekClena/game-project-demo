﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Demina;
using Microsoft.Xna.Framework;

namespace GameAssets
{
    public class AnimationManager
    {
        private HorizontalState PreviousState;
        private AnimationPlayer Animation;

        public AnimationManager(AnimationPlayer animation, HorizontalState state)
        {
            PreviousState = state;
            Animation = animation;
        }

        public void Update(GameTime gameTime, HorizontalState horizontalState, VerticalState verticalState)
        {
            if (horizontalState == HorizontalState.Idle || horizontalState == HorizontalState.Forward) //currently handled states, to be removed
            {
                if ((horizontalState == HorizontalState.Idle && PreviousState != HorizontalState.Idle) || (PreviousState == HorizontalState.Idle && horizontalState != HorizontalState.Idle))
                {
                    Animation.TransitionToAnimation(horizontalState.ToString(), 0.02f);
                }
            }
            PreviousState = horizontalState;
        }
    }
}
