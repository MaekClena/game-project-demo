﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using xTile;
using xTile.Dimensions;
using xTile.Display;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using xTile.Layers;
using GameExternalResources;
using GraphicalUserInterface;
using Constants;
using System.Diagnostics;

namespace GameAssets
{
    public enum StageState { Running, Paused };

    public class Stage : DrawableGameComponent
    {
        //public PlayableStageDefinition Attributes { get; set; }
        public String NextStageLocation { get; set; }
        public StageState State { get; set; }

        public string CharacterDefinitionLocation { get; set; }
        public Vector2 CharacterInitialPosition { get; set; }
        public string TileMapLocation { get; set; }

        public Character Character { get; set; }
        private Map TileMap;
        private IDisplayDevice TileMapDisplayDevice;
        private xTile.Dimensions.Rectangle TileMapViewport;
        private CollisionManager CollisionManager;

        RenderTarget2D RenderTarget;
        public MenuAbstract PauseMenu { get; set; }

        //List<MovableObject>
        //List<NonMovableObject>

        public Stage(Game game, StageDefinition stageDefinition, GraphicalUserInterface.MenuAbstract.MenuQuitEventHandler OnQuit, GameSettings gameSettings)
            : base(game)
        {
            //Attributes = stageDefinition;
            NextStageLocation = stageDefinition.NextStageLocation;
            State = StageState.Running;
            CharacterDefinitionLocation = stageDefinition.CharacterDefinitionLocation;
            CharacterInitialPosition = stageDefinition.CharacterInitialPosition;
            TileMapLocation = stageDefinition.TileMapLocation;
            PauseMenu = new PauseMenu(Game, () => State = StageState.Running, OnQuit, gameSettings);
            LoadContent();
        }

        protected override void LoadContent()
        {
            Character = new Character(Game, CharacterInitialPosition, Game.Content.Load<CharacterDefinition>(@CharacterDefinitionLocation));
            TileMapDisplayDevice = new XnaDisplayDevice(Game.Content, Game.GraphicsDevice);
            //TileMap = new Map();
            TileMap = Game.Content.Load<Map>(TileMapLocation);
            TileMap.LoadTileSheets(TileMapDisplayDevice);
            TileMapViewport = new xTile.Dimensions.Rectangle(new Size(Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height));

            List<Layer> collisionLayers = new List<Layer>();
            xTile.ObjectModel.PropertyValue value;
            foreach (Layer layer in TileMap.Layers)
            {
                if (layer.Properties.TryGetValue("Collidable", out value) && value == "True")
                {
                    collisionLayers.Add(layer);
                }
                if (layer.Properties.TryGetValue("Front", out value) && value == "true")
                {
                    //handle draw event
                }
            }
            //collisionLayers.Reverse(); //better solution somewhere in this world
            CollisionManager = new CollisionManager(collisionLayers);

            RenderTarget = new RenderTarget2D(Game.GraphicsDevice, Game.GraphicsDevice.PresentationParameters.BackBufferWidth, Game.GraphicsDevice.PresentationParameters.BackBufferHeight, false, Game.GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.Depth24);
        }

        public override void Update(GameTime gameTime)
        {
            TileMap.Update(gameTime.ElapsedGameTime.Milliseconds);
            Character.Update(gameTime);
            CollisionManager.CheckForCollision(gameTime, Character.HitBox, Character.Position, Character.Velocity);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            switch (this.State)
            {
                case StageState.Running :
                    /*Reflection POC
                    Game.GraphicsDevice.SetRenderTarget(this.RenderTarget);
                    Game.GraphicsDevice.Clear(Color.Transparent);
                    Character.Draw(gameTime, spriteBatch);
                    Game.GraphicsDevice.SetRenderTarget(null);*/

                    TileMap.Draw(TileMapDisplayDevice, TileMapViewport);
                    
                    //Draw hitbox
                    //var t = new Texture2D(this.context.application.GraphicsDevice, 1, 1);
                    //t.SetData(new[] { Color.Red });
                    //spriteBatch.Draw(t, new Microsoft.Xna.Framework.Rectangle(120, 384, 60, 1), Color.Red);
                    Character.Draw(gameTime, spriteBatch);

                    //Draw reflection
                    //spriteBatch.Draw(renderTarget, new Vector2(0, 350), null, Color.White * 0.5f, 0, Vector2.Zero, 1, SpriteEffects.FlipVertically, 0);
                    break;
                case StageState.Paused :
                    PauseMenu.Draw(gameTime, spriteBatch);
                    break;
            }
        }
    }
}
