﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAssets
{
    public enum HorizontalState { Idle, Forward, FastForward, Backward };

    interface IPlayable
    {
        HorizontalState HorizontalState { get; set; }
        float Acceleration { get; set; } //u.ms^-2
        float Deceleration { get; set; } //u.ms^-2
        float MaxForwardSpeed { get; set; } //u.ms^-1
        float MaxBackwardSpeed { get; set; } //u.ms^-1

        //add readinputs
    }
}
