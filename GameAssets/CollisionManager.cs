﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using xTile.Layers;
using xTile.Dimensions;
using xTile.Tiles;
using System.Diagnostics;
using GameAssets;

namespace GameAssets
{
    public class CollisionManager
    {
        private List<Layer> CollisionLayers;

        public CollisionManager(List<Layer> layers)
        {
            CollisionLayers = layers;
        }

        private bool Collision(Location location/*, out Location tileLocation, out Size tileSize*/)
        {
            //TODO: test if character is out of bounds ?
            //tileLocation = new Location();
            //tileSize = new Size();
            foreach (Layer Layer in CollisionLayers)
            {
                Location tileLocation = Layer.GetTileLocation(location);
                Tile tile = Layer.Tiles[tileLocation];
                if (tile != null)
                {
                    //tileLocation = new Location(tileLocation.X * layer.TileWidth, tileLocation.Y * layer.TileHeight);
                    //tileSize = layer.TileSize;
                    return true;
                }
            }
            return false;
        }

        public void CheckForCollision(GameTime gameTime, List<Microsoft.Xna.Framework.Rectangle> hitBox, Vector2 position, Vector2 velocity)
        {
            //WHERE DIRECTION BE?
            foreach (Microsoft.Xna.Framework.Rectangle Rectangle in hitBox)
            {
                float x, y;
                x = position.X + Rectangle.X;
                y = position.Y + Rectangle.Y + velocity.Y * gameTime.ElapsedGameTime.Milliseconds;
                if (Collision(new Location((int)x, (int)y))
                    || Collision(new Location((int)x, (int)(y + Rectangle.Height)))
                    || Collision(new Location((int)(x + Rectangle.Width), (int)y))
                    || Collision(new Location((int)(x + Rectangle.Width), (int)(y + Rectangle.Height))))
                {
                    velocity.Y = 0; //doesn't work
                }
                else
                {
                    //set vertical state to falling (create Movable and MovableState)
                }
                x = position.X + Rectangle.X + velocity.X * gameTime.ElapsedGameTime.Milliseconds;
                y = position.Y + Rectangle.Y;
                if (Collision(new Location((int)x, (int)y))
                    || Collision(new Location((int)x, (int)(y + Rectangle.Height)))
                    || Collision(new Location((int)(x + Rectangle.Width), (int)y))
                    || Collision(new Location((int)(x + Rectangle.Width), (int)(y + Rectangle.Height))))
                {
                    velocity.X = 0;
                }
                x = position.X + Rectangle.X + velocity.X * gameTime.ElapsedGameTime.Milliseconds;
                y = position.Y + Rectangle.Y + velocity.Y * gameTime.ElapsedGameTime.Milliseconds;
                if (Collision(new Location((int)x, (int)y))
                    || Collision(new Location((int)x, (int)(y + Rectangle.Height)))
                    || Collision(new Location((int)(x + Rectangle.Width), (int)y))
                    || Collision(new Location((int)(x + Rectangle.Width), (int)(y + Rectangle.Height))))
                {
                    velocity.X = 0;
                    velocity.Y = 0;
                }
            }
        }

        //WHAT THE FUCKING FUkCKJSKJLkjlnlskdnqklsj dqjef ia
        public void Update(GameTime gameTime)
        {
            /*foreach (Microsoft.Xna.Framework.Rectangle Rectangle in Character.HitBox)
            {
                foreach (Layer Layer in Layers)
                {
                    Location TileLocation;
                    Size TileSize;

                    //LEFT
                    if (Layer.Tiles[] != null

                    //if (SideCollision(new Location(Character.Position.X + Rectangle.X), new Location((int)Character.Position.X, (int)Character.Position.Y + (int)Character.HitBox.Height), out TileLocation1, out TileSize1, out TileLocation2, out TileSize2))
                    if (Collision(new Location((int)Character.Position.X, (int)Character.Position.Y), out TileLocation, out TileSize))
                    {
                        //TOP LEFT
                        Character.MoveTo();
                    }

                    if (SideCollision(new Location((int)Character.Position.X + (int)Character.HitBox.Width, (int)Character.Position.Y), new Location((int)Character.Position.X + (int)Character.HitBox.Width, (int)Character.Position.Y + (int)Character.HitBox.Height), out TileLocation1, out TileSize1, out TileLocation2, out TileSize2))
                    {
                        //RIGHT
                        if (TileLocation1.X < TileLocation2.X)
                        {
                            Character.MoveTo(TileLocation1.X - TileSize1.Width, Character.Position.Y);
                        }
                        else
                        {
                            Character.MoveTo(TileLocation2.X - TileSize2.Width, Character.Position.Y);
                        }
                        //character.CurrentHorizontalState = CharacterHorizontalState.Idle;
                    }

                    if (SideCollision(new Location((int)Character.Position.X, (int)Character.Position.Y), new Location((int)Character.Position.X + (int)Character.HitBox.Width, (int)Character.Position.Y), out TileLocation1, out TileSize1, out TileLocation2, out TileSize2))
                    {
                        //TOP
                        if (TileLocation1.Y < TileLocation2.Y)
                        {
                            Character.MoveTo(Character.Position.X, TileLocation1.Y + TileSize1.Height);
                        }
                        else
                        {
                            Character.MoveTo(Character.Position.X, TileLocation2.Y + TileSize2.Height);
                        }
                        //character.CurrentVerticalState = CharacterVerticalState.Idle;
                    }

                    if (SideCollision(new Location((int)Character.Position.X, (int)Character.Position.Y + (int)Character.HitBox.Height), new Location((int)Character.Position.X + (int)Character.HitBox.Width, (int)Character.Position.Y + (int)Character.HitBox.Height), out TileLocation1, out TileSize1, out TileLocation2, out TileSize2))
                    {
                        //BOTTOM
                        if (TileLocation1.Y > TileLocation2.Y)
                        {
                            Character.MoveTo(Character.Position.X, TileLocation1.Y - TileSize1.Height);
                        }
                        else
                        {
                            Character.MoveTo(Character.Position.X, TileLocation2.Y - TileSize2.Height);
                        }
                        //character.CurrentVerticalState = CharacterVerticalState.Idle;
                    }
                    else
                    {
                        Character.CurrentVerticalState = CharacterVerticalState.Falling;
                    }
                }
            }*/
        }
    }
}
