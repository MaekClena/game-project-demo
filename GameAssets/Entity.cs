﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameAssets
{
    public abstract class Entity : DrawableGameComponent
    {
        public Vector2 Position { get; set; }
        public sbyte Direction { get; set; }
        public List<Rectangle> HitBox { get; set; }

        public Entity(Game game, Vector2 position, sbyte direction, List<Rectangle> hitBox)
            : base(game)
        {
            Position = position;
            Direction = direction;
            HitBox = hitBox;
        }

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}
