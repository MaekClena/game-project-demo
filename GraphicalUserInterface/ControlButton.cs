﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GraphicalUserInterface
{
    public class ControlButton : ControlAbstract
    {
        public ControlButton(Game game, String label, Vector2 position, SpriteFont font, SelectEventHandler action)
            : base(game, label, position, font)
        {
            OnSelect += action;
            //LoadContent();
        }

        protected override void LoadContent()
        {
            throw new NotImplementedException();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            switch (CurrentState)
            {
                case ControlState.Idle :
                    spriteBatch.DrawString(Font, Label, Position, Color.White);
                    break;
                case ControlState.Hover :
                    spriteBatch.DrawString(Font, Label, Position, Color.Gray);
                    break;
            }
        }

        public override void ReadInput(MouseState currentMouseState, MouseState previousMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            if (currentMouseState.X > Position.X
                && currentMouseState.X < Position.X + MeasureString.X
                && currentMouseState.Y > Position.Y
                && currentMouseState.Y < Position.Y + MeasureString.Y)
            {
                CurrentState = ControlState.Hover;
                if (currentMouseState.LeftButton == ButtonState.Released && previousMouseState.LeftButton == ButtonState.Pressed)
                {
                    Selected();
                }
            }
            else
            {
                CurrentState = ControlState.Idle;
            }
        }
    }
}
