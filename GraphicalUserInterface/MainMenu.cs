﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameExternalResources;
using Microsoft.Xna.Framework.Input;
using Constants;

namespace GraphicalUserInterface
{
    public class MainMenu : MenuAbstract
    {
        Layout MainLayout;
        Layout SettingsLayout;

        public MainMenu(Game game, GraphicalUserInterface.ControlAbstract.SelectEventHandler onStart, GraphicalUserInterface.Layout.LayoutQuitEventHandler onSave, GameSettings gameSettings, Dictionary<String, Keys> keyBinds)
            : base(game, game.Exit)
        {
            SettingsLayout = new Layout(Game, this, Parameters.MenuBackgroundLocation);
            SettingsLayout.Controls.Add(new ControlInput(Game, "Walk forward", new Vector2(300, 50), Font, keyBinds["Forward"]));
            SettingsLayout.Controls.Add(new ControlInput(Game, "Walk backward", new Vector2(300, 100), Font, keyBinds["Backward"]));
            SettingsLayout.Controls.Add(new ControlInput(Game, "Jump", new Vector2(300, 150), Font, keyBinds["Up"]));
            SettingsLayout.Controls.Add(new ControlSlide(Game, "Music volume", new Vector2(300, 250), Font, gameSettings.MusicVolume));
            SettingsLayout.Controls.Add(new ControlSlide(Game, "Effects volume", new Vector2(300, 300), Font, gameSettings.EffectsVolume));
            SettingsLayout.Controls.Add(new ControlButton(Game, "Return to menu", new Vector2(300, 350), Font, SettingsLayout.Quit));
            SettingsLayout.OnQuit += onSave;

            MainLayout = new Layout(Game, this, Parameters.MenuBackgroundLocation);
            MainLayout.Controls.Add(new ControlButton(Game, "Resume game", new Vector2(300, 150), Font, onStart));
            MainLayout.Controls.Add(new ControlButton(Game, "Settings", new Vector2(300, 200), Font, GoToSettings));
            MainLayout.Controls.Add(new ControlButton(Game, "Quit", new Vector2(300, 250), Font, MainLayout.Quit));

            MainLayout.AddChild(SettingsLayout);

            CurrentLayout = MainLayout;
        }



        public void GoToSettings()
        {
            MainLayout.GoToChild(SettingsLayout);
        }
    }
}