﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GraphicalUserInterface
{
    public class ControlCheckBox : ControlAbstract
    {
        public ControlCheckBox(Game game, String label, Vector2 position, SpriteFont font)
            : base(game, label, position, font)
        {

        }

        protected override void LoadContent()
        {
            throw new NotImplementedException();
        }

        public override void ReadInput(MouseState currentMouseState, MouseState previsouMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }
    }
}
