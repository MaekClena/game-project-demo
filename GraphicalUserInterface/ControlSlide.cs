﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameExternalResources;
using Constants;

namespace GraphicalUserInterface
{
    public class ControlSlide : ControlAbstract
    {
        Setting Setting { get; set; }
        public static int SlideHeight = 32;
        public static int SlideWidth = 192;
        public static int ButtonDimension = 32;
        private Texture2D SlideTexture;

        public ControlSlide(Game game, String label, Vector2 position, SpriteFont font, Setting setting)
            : base(game, label, position, font)
        {
            Setting = setting;
            LoadContent();
        }

        protected override void LoadContent()
        {
            SlideTexture = Game.Content.Load<Texture2D>(@Parameters.SlideTextureLocation);
        }

        public override void ReadInput(MouseState currentMouseState, MouseState previousMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            if (currentMouseState.LeftButton == ButtonState.Pressed)
            {
                switch (this.CurrentState)
                {
                    case ControlState.Selected:
                            Setting.Value = (currentMouseState.X - (Position.X + MeasureString.X + 20 + 10)) / (ControlSlide.SlideWidth - 20);
                            //régler le décalage souris/control
                            if (Setting.Value < 0.0)
                            {
                                Setting.Value = 0.0f;
                            }
                            else if (Setting.Value > 1.0)
                            {
                                Setting.Value = 1.0f;
                            }
                        break;
                    case ControlState.Idle:
                        if (previousMouseState.LeftButton == ButtonState.Released
                            && currentMouseState.X > Position.X + MeasureString.X + 20 
                            && currentMouseState.X < Position.X + MeasureString.X + 20 + ControlSlide.SlideWidth 
                            && currentMouseState.Y > Position.Y
                            && currentMouseState.Y < Position.Y + ControlSlide.SlideHeight)
                        {
                            Selected();
                        }
                        break;
                }
            }
            else
            {
                this.CurrentState = ControlState.Idle;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, this.Label, this.Position, Color.White);
            spriteBatch.Draw(SlideTexture, Position + new Vector2(MeasureString.X + 20, 0), new Rectangle(0, 0, SlideWidth, SlideHeight), Color.White);
            switch (this.CurrentState)
            {
                case ControlState.Idle:
                    spriteBatch.Draw(SlideTexture, Position + new Vector2(MeasureString.X + 20 + Setting.Value * (SlideWidth - ButtonDimension), 0), new Rectangle(SlideWidth, 0, ButtonDimension, ButtonDimension), Color.White);
                    break;
                case ControlState.Selected:
                    spriteBatch.Draw(SlideTexture, Position + new Vector2(MeasureString.X + 20 + Setting.Value * (SlideWidth - ButtonDimension), 0), new Rectangle(SlideWidth + ButtonDimension, 0, ButtonDimension, ButtonDimension), Color.White);
                    break;
            }
        }
    }
}
