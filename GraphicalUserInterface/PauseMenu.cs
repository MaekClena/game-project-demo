﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameExternalResources;
using Constants;

namespace GraphicalUserInterface
{
    public class PauseMenu : MenuAbstract
    {
        Layout MainLayout;

        public PauseMenu(Game game, GraphicalUserInterface.ControlAbstract.SelectEventHandler onResume, MenuQuitEventHandler onQuit, GameSettings gameSettings)
            : base(game, onQuit)
        {
            MainLayout = new Layout(Game, this, Parameters.MenuBackgroundLocation);
            MainLayout.Controls.Add(new ControlSlide(Game, "Music volume", new Vector2(300, 200), Font, gameSettings.MusicVolume));
            MainLayout.Controls.Add(new ControlSlide(Game, "Effects volume", new Vector2(300, 250), Font, gameSettings.EffectsVolume));
            MainLayout.Controls.Add(new ControlButton(Game, "Resume game", new Vector2(300, 300), Font, onResume));
            MainLayout.Controls.Add(new ControlButton(Game, "Quit game", new Vector2(300, 350), Font, MainLayout.Quit));

            CurrentLayout = MainLayout;
        }
    }
}