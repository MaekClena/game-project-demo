﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GraphicalUserInterface
{
    public class Layout : DrawableGameComponent
    {
        public List<ControlAbstract> Controls { get; set; }
        public MenuAbstract Context { get; set; }
        public Layout Parent { get; set; }
        public List<Layout> Children { get; set; }
        public Texture2D Background { get; set; }
        public String BackgroundLocation { get; set; }

        public delegate void LayoutQuitEventHandler();
        public event LayoutQuitEventHandler OnQuit;

        public delegate void LayoutGoToChildEventHandler(Layout layout);
        public event LayoutGoToChildEventHandler OnGoToChild;

        public Layout(Game game, MenuAbstract context, String backgroundLocation)
            : base(game)
        {
            Controls = new List<ControlAbstract>();
            Parent = null;
            Context = context;
            Children = new List<Layout>();
            OnGoToChild += Context.GoToChildLayout;
            OnQuit += Context.QuitLayout;
            BackgroundLocation = backgroundLocation;
            LoadContent();
        }

        protected override void LoadContent()
        {
            Background = Game.Content.Load<Texture2D>(@BackgroundLocation);
        }

        public void AddChild(Layout child)
        {
            Children.Add(child);
            child.Parent = this;
        }

        public void Quit()
        {
            if (OnQuit != null)
            {
                OnQuit();
            }
        }

        public void GoToChild(Layout child)
        {
            if (OnGoToChild != null && Children.Contains(child))
            {
                OnGoToChild(child);
            }
        }

        public void ReadInput(MouseState currentMouseState, MouseState previsouMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            foreach (ControlAbstract Control in Controls)
            {
                Control.ReadInput(currentMouseState, previsouMouseState, currentKeyboardState, previousKeyboardState);
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Background, Background.Bounds, Color.White);
            foreach (ControlAbstract Control in Controls)
            {
                Control.Draw(gameTime, spriteBatch);
            }
        }
    }
}
