﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Constants;

namespace GraphicalUserInterface
{
    public abstract class MenuAbstract : DrawableGameComponent
    {
        //TODO: add keyboard control
        //TODO: not load multiple times the images (slide texture)
        //TODO: serialize menu style
        public Layout CurrentLayout { get; set; }
        public SpriteFont Font { get; set; }

        public delegate void MenuQuitEventHandler();
        public event MenuQuitEventHandler QuitMenu;

        public MenuAbstract(Game game, MenuQuitEventHandler OnQuit)
            : base(game)
        {
            QuitMenu += OnQuit;
            LoadContent();
        }

        protected override void LoadContent()
        {
            Font = Game.Content.Load<SpriteFont>(@Parameters.FontLocation);
        }

        public void GoToChildLayout(Layout layout)
        {
            if (layout != null)
            {
                CurrentLayout = layout;
            }
        }

        public void QuitLayout()
        {
            if (CurrentLayout.Parent != null)
            {
                CurrentLayout = CurrentLayout.Parent;
            }
            else
            {
                QuitMenu();
            }
        }

        public void ReadInput(MouseState currentMouseState, MouseState previsouMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            CurrentLayout.ReadInput(currentMouseState, previsouMouseState, currentKeyboardState, previousKeyboardState);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            CurrentLayout.Draw(gameTime, spriteBatch);
        }
    }
}
