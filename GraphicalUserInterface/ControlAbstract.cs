﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GraphicalUserInterface
{
    public enum ControlState { Idle, Hover, Selected };

    public abstract class ControlAbstract : DrawableGameComponent
    {
        public ControlState CurrentState { get; set; }
        public string Label { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 MeasureString { get; set; }
        public SpriteFont Font { get; set; }

        public delegate void SelectEventHandler();
        public event SelectEventHandler OnSelect;

        public ControlAbstract(Game game, String label, Vector2 position, SpriteFont font)
            : base(game)
        {
            CurrentState = ControlState.Idle;
            Label = label;
            Position = position;
            MeasureString = font.MeasureString(label);
            Font = font;
            //LoadContent();
        }

        public virtual void Selected()
        {
            CurrentState = ControlState.Selected;
            if (OnSelect != null)
            {
                OnSelect();
            }
        }

        protected override abstract void LoadContent();

        public abstract void ReadInput(MouseState currentMouseState, MouseState previsouMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState);

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}
