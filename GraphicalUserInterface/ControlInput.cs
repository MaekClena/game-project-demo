﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GraphicalUserInterface
{
    public class ControlInput : ControlAbstract
    {
        public Keys Key { get; set; }
        private Vector2 Offset;

        public ControlInput(Game game, String label, Vector2 position, SpriteFont font, Keys key)
            : base(game, label, position, font)
        {
            Offset = new Vector2(20, 0);
            Key = key; //doesn't change the dictionnary, use dictionnary instead of enum ?
            //LoadContent();
        }

        protected override void LoadContent()
        {
            throw new NotImplementedException();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, Label, Position, Color.White);
            switch(CurrentState)
            {
                case ControlState.Idle :
                    spriteBatch.DrawString(Font, Key.ToString(), Position + new Vector2(MeasureString.X + 20, 0), Color.White);
                    break;
                case ControlState.Hover : 
                    break;
                case ControlState.Selected :
                    spriteBatch.DrawString(Font, Key.ToString(), Position + new Vector2(MeasureString.X + 20, 0), Color.Red);
                    break;
            }
        }

        public override void ReadInput(MouseState currentMouseState, MouseState previousMouseState, KeyboardState currentKeyboardState, KeyboardState previousKeyboardState)
        {
            if (currentMouseState.LeftButton == ButtonState.Released && previousMouseState.LeftButton == ButtonState.Pressed)
            {
                if (CurrentState == ControlState.Idle)
                {
                    if (currentMouseState.X > Position.X + MeasureString.X + 20
                        && currentMouseState.X < Position.X + MeasureString.X + 20 + Font.MeasureString(Key.ToString()).X
                        && currentMouseState.Y > Position.Y
                        && currentMouseState.Y < Position.Y + Font.MeasureString(Key.ToString()).Y)
                    {
                        Selected();
                    }
                }
                else
                {
                    CurrentState = ControlState.Idle;
                }
            }
            if (CurrentState == ControlState.Selected && currentKeyboardState.GetPressedKeys().Length > 0)
            {
                Key = currentKeyboardState.GetPressedKeys()[0];
                CurrentState = ControlState.Idle;
            }
        }
    }
}
