﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Constants
{
    public class Parameters
    {
        public const string SettingsLocation = @"config\game";
        public const string KeyBindsLocation = @"config\inputs";
        public const string SlideTextureLocation = @"graphics\images\control_slide";
        public const string MenuBackgroundLocation = @"graphics\images\menu_background";
        public const string PointerTextureLocation = @"graphics\images\pointer";
        public const string FontLocation = @"graphics\fonts\defaultfont";
        public const string SaveFileLocation = @"savegame.sav";
        public const string FirstStageLocation = @"stages\stage01";
    }
}
